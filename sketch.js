var width = window.innerWidth
var height = window.innerHeight

// euclidean distance
let dist = ((p1, p2) => sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))

// angle between y-axis and the vector that spans [p1, p2]
let angle = (p1, p2) => Math.atan2(p2[1] - p1[1], p2[0] - p1[0])

// move a point or a triangle by v
let move_thing = (xys, v) => xys.map((e, i) => e + (i%2==0 ? v[0] : v[1]))

// rotate a point or a triangle around point c by theta
function rotate_thing(xys, c, th) {
  let at_origin = move_thing(xys, [-c[0], -c[1]])
  let rotated = at_origin.map((e, i, l) =>
    i%2 == 0
    ? e * cos(th) - l[i+1] * sin(th)  // ys
    : e * cos(th) + l[i-1] * sin(th)  // ys
  )
  return move_thing(rotated, c)
}

// ...I miss python
let range = ((n) => [...Array(n).keys()])

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
}

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL)
  colorMode(HSB, 255)
  frameRate(30)

}


let fg = [190, 100, 100]
let bg = [135, 100, 100]
let blobcolor = [190, 180, 100]
var blob = [0, 0]
var blobdir = Math.PI
var blobnoise = 1
var blobturnspeed = 0.10
var blobspeed = 0.5
var t = 0
function draw() {
  rotateX(PI/4)
  background(...bg)
  blendMode(SUBTRACT)
  fill(...fg)

    // circles
  for (var x=(-width/2); x < width/2 + width/20; x += 50) {
    for (var y=(-height/2); y < height/2 + height/10; y += 50) {
      noStroke()
      fill(fg)
      let r = sin(x + t) * 20
      push()
      // fill(map(x))
      // normalMaterial()
      translate(-x,-y)
      push()
      rotateX(PI/2)
      let h = r*10 + y
      stroke(map(h,-200 +(-height/2), 200+(height/2), 0, 255), 255, 255)
      fill(0,0,0)
      cylinder(20,h,4)
      // cylinder(30,(r*10 + y)*0.9,4)
      // cylinder(30,(r*10 + y)*0.8,4)
      pop()
      pop()
    }
  }

  // draw blob
  fill(blobcolor)
  push()
  translate(-blob[0], -blob[1])
  rotateZ(t)
  rotateX(PI/2)
  translate(0, 200)
  stroke((t%2)*128, 255, 255)
  strokeWeight(4)
  noFill()
  sphere(50, 4,4)
  stroke(0, 0,0)
  strokeWeight(1)
  noFill()
  sphere(50, 4,4)
  pop()

  // move blob
  blob[0] = blob[0] + (cos(blobdir) * blobspeed)
  blob[1] = blob[1] + (sin(blobdir) * blobspeed)
  blobdir += (noise(t * blobnoise) - 0.5) * blobturnspeed

  t += 0.01
}
